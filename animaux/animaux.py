#!/usr/bin/env python3


from gpiozero import Button
import subprocess

from signal import pause

def ane():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/ane.mp3', shell=True)

def canard():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/canard.mp3', shell=True)

def cochon():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/cochon.mp3', shell=True)

def oie():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/oie.mp3', shell=True)

def poule():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/poule.mp3', shell=True)

def vache():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/vache.mp3', shell=True)

def mouton():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/mouton.mp3', shell=True)

def cheval():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/animaux/cheval.mp3', shell=True)

def stop():
	subprocess.run('mocp --stop', shell=True)

ane_btn = Button(2)
canard_btn = Button(3)
cochon_btn = Button(4)
oie_btn = Button(17)
poule_btn = Button(11)
vache_btn = Button(22)
mouton_btn = Button(10)
cheval_btn = Button(9)
stop_btn = Button(27)



ane_btn.when_pressed = ane
canard_btn.when_pressed = canard
cochon_btn.when_pressed = cochon
oie_btn.when_pressed = oie
poule_btn.when_pressed = poule
vache_btn.when_pressed = vache
mouton_btn.when_pressed = mouton
cheval_btn.when_pressed = cheval
stop_btn.when_pressed = stop

pause()
