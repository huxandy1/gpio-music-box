# Pyhton scripts that use button pressed to play songs and sounds.

### Bash scripts that reload pythons scripts to use others sounds.

##### **Matériel requis :**
1. Raspberry Pi 3/4 B/B+ & a SD card & a power supply suffisante
2. Cables
3. Boutons poussoirs
4. Carte SD de 16Go au moins
5. Enceintes

##### **Première étape :**
1. Graver l'image disque de Raspberry OS sur la carte SD https://downloads.raspberrypi.org/raspios_full_armhf_latest
2. Avant de la retirer de l'ordinateur :
- dans la **partition** /boot, créer le fichier ssh

    sudo touch ssh

- dans /etc/wpa_supplicant/wpa_supplicant.conf

`    nano /etc/wpa_supplicant/wpa_supplicant.conf`

et ajouter :
```
country=fr
    update_config=1
    ctrl_interface=/var/run/wpa_supplicant
    network={
     scan_ssid=1
     ssid="NomDuRéseau"
     psk="cléDeSécurité"
    }
```


3. Insérer la carte et brancher le Raspberry, attendre de le voir apparaître sur le réseau.

4. Se connecter en SSH au Raspberry https://raspberry-lab.fr/Configuration/Connexion-en-ssh-au-Raspberry-Pi/

5. Une fois connecté, mettre à jour le raspberrypi :

`sudo apt update && sudo apt upgrade -y`

puis redémarrage du Raspberry :

`sudo reboot`

puis élargir le système de fichier : 

`sudo raspi-config`

puis naviguer vers Advanced Options >> Expand Filesystem.

puis de nouveau : 

Interface Option >> SPI >> Enable

Redémarrer le raspberry quand on vous le proposera.

6. Télécharger le projet :

`    git clone https://gitlab.com/huxandy1/buttons.git`

Toute la librairie et les scripts python sont ajoutés à votre dossier, ainsi que les script bash de modification du daemon python et des références musiques.

7. Normalement, tous les modules python appelés sont disponibles dans la version full de Raspberry OS. Mais il vaut mieux vérifier :
`sudo apt install python3-pip`

```
pip3  install gpiozero
pip3 install subprocess
pip3 install pygame
```
8. Branchement des boutons :
Il est ensuite necessaire de passer au branchement des boutons.
J'ai utilisé les boutons de la première colonne de pins du GPIO. Mais n'importe quel pin fait l'affaire, pourvu que ce ne soit pas les alimentations ou la terre (ground).
Il est possible d'utiliser un domino pour rejoindre tous les ground ensemble (en vert sur le schema)

![alt text](https://gitlab.com/huxandy1/buttons/-/raw/master/buttons.jpg "Branchement des boutons")

9. Pour tester les scripts :
Brancher un écran et se rendre dans les dossier de présence des scripts python, puis faire : 
`python3 /chemin/vers/le/script.py`

Le script est censé se lancer et les premières musiques censées se jouer.
Un `Ctrl` + `C` est necessaire à ce stade pour évacuer le script python.


