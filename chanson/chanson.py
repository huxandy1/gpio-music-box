#!/usr/bin/env python3


from gpiozero import Button
import subprocess

from signal import pause

def chansondouce():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/chansondouce.mp3', shell=True)

def dinotrain():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/dinotrain.mp3', shell=True)

def giletjaune():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/giletjaune.mp3', shell=True)

def harrypotter():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/harrypotter.mp3', shell=True)

def rainbow():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/rainbow.mp3', shell=True)

def throneroom():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/throneroom.mp3', shell=True)

def trex():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/trex.mp3', shell=True)

def wakawaka():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/chanson/wakawaka.mp3', shell=True)

def stop():
	subprocess.run('mocp --stop', shell=True)

chansondouce_btn = Button(2)
dinotrain_btn = Button(3)
giletjaune_btn = Button(4)
harrypotter_btn = Button(17)
rainbow_btn = Button(11)
throneroom_btn = Button(22)
trex_btn = Button(10)
wakawaka_btn = Button(9)
stop_btn = Button(27)



chansondouce_btn.when_pressed = chansondouce
dinotrain_btn.when_pressed = dinotrain
giletjaune_btn.when_pressed = giletjaune
harrypotter_btn.when_pressed = harrypotter
rainbow_btn.when_pressed = rainbow
throneroom_btn.when_pressed = throneroom
trex_btn.when_pressed = trex
wakawaka_btn.when_pressed = wakawaka
stop_btn.when_pressed = stop

pause()
