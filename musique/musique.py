#!/usr/bin/env python3


from gpiozero import Button
import subprocess

from signal import pause

def chandelier():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/chandelier.mp3', shell=True)

def papaoutai():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/papaoutai.mp3', shell=True)

def jain():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/jain.mp3', shell=True)

def roxanne():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/roxanne.mp3', shell=True)

def papanoel():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/papanoel.mp3', shell=True)

def laccygne():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/laccygne.mp3', shell=True)

def radiogaga():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/radiogaga.mp3', shell=True)

def canabis():
	subprocess.run('mocp --playit /home/pi/gpio-music-box/musique/canabis.mp3', shell=True)

def stop():
	subprocess.run('mocp --stop', shell=True)

chandelier_btn = Button(2)
papaoutai_btn = Button(3)
jain_btn = Button(4)
roxanne_btn = Button(17)
papanoel_btn = Button(11)
laccygne_btn = Button(22)
radiogaga_btn = Button(10)
canabis_btn = Button(9)
stop_btn = Button(27)



chandelier_btn.when_pressed = chandelier
papaoutai_btn.when_pressed = papaoutai
jain_btn.when_pressed = jain
roxanne_btn.when_pressed = roxanne
papanoel_btn.when_pressed = papanoel
laccygne_btn.when_pressed = laccygne
radiogaga_btn.when_pressed = radiogaga
canabis_btn.when_pressed = canabis
stop_btn.when_pressed = stop

pause()
